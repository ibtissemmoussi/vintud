PGDMP         )                w            vintud    10.11    10.11 '               0    0    ENCODING    ENCODING        SET client_encoding = 'UTF8';
                       false                       0    0 
   STDSTRINGS 
   STDSTRINGS     (   SET standard_conforming_strings = 'on';
                       false                       0    0 
   SEARCHPATH 
   SEARCHPATH     8   SELECT pg_catalog.set_config('search_path', '', false);
                       false                       1262    16740    vintud    DATABASE     �   CREATE DATABASE vintud WITH TEMPLATE = template0 ENCODING = 'UTF8' LC_COLLATE = 'French_France.1252' LC_CTYPE = 'French_France.1252';
    DROP DATABASE vintud;
             postgres    false                        2615    2200    public    SCHEMA        CREATE SCHEMA public;
    DROP SCHEMA public;
             postgres    false                       0    0    SCHEMA public    COMMENT     6   COMMENT ON SCHEMA public IS 'standard public schema';
                  postgres    false    3                        3079    12924    plpgsql 	   EXTENSION     ?   CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;
    DROP EXTENSION plpgsql;
                  false                       0    0    EXTENSION plpgsql    COMMENT     @   COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';
                       false    1            �            1259    16786    announcement    TABLE     �  CREATE TABLE public.announcement (
    id integer NOT NULL,
    title character(50) NOT NULL,
    description character varying(750) NOT NULL,
    category_id integer NOT NULL,
    price double precision NOT NULL,
    picture oid,
    publication_date timestamp without time zone NOT NULL,
    is_available boolean NOT NULL,
    view_number integer NOT NULL,
    localisation character(100),
    user_id integer NOT NULL
);
     DROP TABLE public.announcement;
       public         postgres    false    3            �            1259    16784    announcement_id_seq    SEQUENCE     �   CREATE SEQUENCE public.announcement_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 *   DROP SEQUENCE public.announcement_id_seq;
       public       postgres    false    203    3                       0    0    announcement_id_seq    SEQUENCE OWNED BY     K   ALTER SEQUENCE public.announcement_id_seq OWNED BY public.announcement.id;
            public       postgres    false    202            �            1259    16778    category    TABLE     �   CREATE TABLE public.category (
    id integer NOT NULL,
    name character(30) NOT NULL,
    description character varying(255) NOT NULL
);
    DROP TABLE public.category;
       public         postgres    false    3            �            1259    16776    category_id_seq    SEQUENCE     �   CREATE SEQUENCE public.category_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 &   DROP SEQUENCE public.category_id_seq;
       public       postgres    false    201    3                       0    0    category_id_seq    SEQUENCE OWNED BY     C   ALTER SEQUENCE public.category_id_seq OWNED BY public.category.id;
            public       postgres    false    200            �            1259    16743    role    TABLE     V   CREATE TABLE public.role (
    id integer NOT NULL,
    nom character(30) NOT NULL
);
    DROP TABLE public.role;
       public         postgres    false    3            �            1259    16741    role_id_seq    SEQUENCE     �   CREATE SEQUENCE public.role_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 "   DROP SEQUENCE public.role_id_seq;
       public       postgres    false    197    3                       0    0    role_id_seq    SEQUENCE OWNED BY     ;   ALTER SEQUENCE public.role_id_seq OWNED BY public.role.id;
            public       postgres    false    196            �            1259    16762    users    TABLE     M  CREATE TABLE public.users (
    id integer NOT NULL,
    firstname character(80) NOT NULL,
    name character(80) NOT NULL,
    pseudo character(80) NOT NULL,
    mail character(80) NOT NULL,
    u_password character(80) NOT NULL,
    phone character(14) NOT NULL,
    address character(80) NOT NULL,
    role_id integer NOT NULL
);
    DROP TABLE public.users;
       public         postgres    false    3            �            1259    16760    users_id_seq    SEQUENCE     �   CREATE SEQUENCE public.users_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 #   DROP SEQUENCE public.users_id_seq;
       public       postgres    false    199    3                       0    0    users_id_seq    SEQUENCE OWNED BY     =   ALTER SEQUENCE public.users_id_seq OWNED BY public.users.id;
            public       postgres    false    198            �
           2604    16789    announcement id    DEFAULT     r   ALTER TABLE ONLY public.announcement ALTER COLUMN id SET DEFAULT nextval('public.announcement_id_seq'::regclass);
 >   ALTER TABLE public.announcement ALTER COLUMN id DROP DEFAULT;
       public       postgres    false    203    202    203            �
           2604    16781    category id    DEFAULT     j   ALTER TABLE ONLY public.category ALTER COLUMN id SET DEFAULT nextval('public.category_id_seq'::regclass);
 :   ALTER TABLE public.category ALTER COLUMN id DROP DEFAULT;
       public       postgres    false    201    200    201            �
           2604    16746    role id    DEFAULT     b   ALTER TABLE ONLY public.role ALTER COLUMN id SET DEFAULT nextval('public.role_id_seq'::regclass);
 6   ALTER TABLE public.role ALTER COLUMN id DROP DEFAULT;
       public       postgres    false    197    196    197            �
           2604    16765    users id    DEFAULT     d   ALTER TABLE ONLY public.users ALTER COLUMN id SET DEFAULT nextval('public.users_id_seq'::regclass);
 7   ALTER TABLE public.users ALTER COLUMN id DROP DEFAULT;
       public       postgres    false    199    198    199                      0    16786    announcement 
   TABLE DATA               �   COPY public.announcement (id, title, description, category_id, price, picture, publication_date, is_available, view_number, localisation, user_id) FROM stdin;
    public       postgres    false    203   ")                 0    16778    category 
   TABLE DATA               9   COPY public.category (id, name, description) FROM stdin;
    public       postgres    false    201   *                 0    16743    role 
   TABLE DATA               '   COPY public.role (id, nom) FROM stdin;
    public       postgres    false    197   �*                 0    16762    users 
   TABLE DATA               g   COPY public.users (id, firstname, name, pseudo, mail, u_password, phone, address, role_id) FROM stdin;
    public       postgres    false    199   �*                  0    0    announcement_id_seq    SEQUENCE SET     A   SELECT pg_catalog.setval('public.announcement_id_seq', 2, true);
            public       postgres    false    202                       0    0    category_id_seq    SEQUENCE SET     =   SELECT pg_catalog.setval('public.category_id_seq', 3, true);
            public       postgres    false    200                        0    0    role_id_seq    SEQUENCE SET     9   SELECT pg_catalog.setval('public.role_id_seq', 1, true);
            public       postgres    false    196            !           0    0    users_id_seq    SEQUENCE SET     :   SELECT pg_catalog.setval('public.users_id_seq', 3, true);
            public       postgres    false    198            �
           2606    16794    announcement announcement_pkey 
   CONSTRAINT     \   ALTER TABLE ONLY public.announcement
    ADD CONSTRAINT announcement_pkey PRIMARY KEY (id);
 H   ALTER TABLE ONLY public.announcement DROP CONSTRAINT announcement_pkey;
       public         postgres    false    203            �
           2606    16783    category category_pkey 
   CONSTRAINT     T   ALTER TABLE ONLY public.category
    ADD CONSTRAINT category_pkey PRIMARY KEY (id);
 @   ALTER TABLE ONLY public.category DROP CONSTRAINT category_pkey;
       public         postgres    false    201            �
           2606    16748    role role_pkey 
   CONSTRAINT     L   ALTER TABLE ONLY public.role
    ADD CONSTRAINT role_pkey PRIMARY KEY (id);
 8   ALTER TABLE ONLY public.role DROP CONSTRAINT role_pkey;
       public         postgres    false    197            �
           2606    16770    users users_pkey 
   CONSTRAINT     N   ALTER TABLE ONLY public.users
    ADD CONSTRAINT users_pkey PRIMARY KEY (id);
 :   ALTER TABLE ONLY public.users DROP CONSTRAINT users_pkey;
       public         postgres    false    199            �
           2606    16800 $   announcement fk_categoryannouncement    FK CONSTRAINT     �   ALTER TABLE ONLY public.announcement
    ADD CONSTRAINT fk_categoryannouncement FOREIGN KEY (category_id) REFERENCES public.category(id);
 N   ALTER TABLE ONLY public.announcement DROP CONSTRAINT fk_categoryannouncement;
       public       postgres    false    2699    203    201            �
           2606    16771    users fk_roleuser    FK CONSTRAINT     o   ALTER TABLE ONLY public.users
    ADD CONSTRAINT fk_roleuser FOREIGN KEY (role_id) REFERENCES public.role(id);
 ;   ALTER TABLE ONLY public.users DROP CONSTRAINT fk_roleuser;
       public       postgres    false    2695    197    199            �
           2606    16795     announcement fk_userannouncement    FK CONSTRAINT        ALTER TABLE ONLY public.announcement
    ADD CONSTRAINT fk_userannouncement FOREIGN KEY (user_id) REFERENCES public.users(id);
 J   ALTER TABLE ONLY public.announcement DROP CONSTRAINT fk_userannouncement;
       public       postgres    false    2697    203    199               �   x���Qj�0�g�:�bbueI/0t
{ꋛh��vh,�L�9r�&l�c�}Lo���"�]/�m��0� V�j��c�0L��}���2��,��Y�az�8�-�э=#GL�o��:��н�+�>��.JS��j���! x��KXO������^y>�1w)aӅi\�|�5�Ŕwޅ�q���`lXf�O����T��g	�%�����I��V+�N�քX         �   x�3�Q��
Q�8��*I�M�+Q�9�2=�H�8�����,	�^��e���W����GД�R���b��_T 6� ���+-+��Z�����T�Z��e��Z\���m(�&g$���q��qqq �,<�            x�3�t��t�Q��b���� }��           x���AN�0E��)|����k��*!U�%��RW�]�$�����HP�����7����R�,ABŶD��F6�6��'m&�|�i�B�v��mH�#���JE�ެ��v�����'B�$>Y�0)��ھ[]\����E���^�<�X3�5�o��@�3�\�^��I|��xP-x���}H�t�2�ů�S��8�s�_?�_�>�������a��7���FO��;ʦ���4�D��/��gG��BJ����     