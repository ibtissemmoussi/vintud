package com.vintud.test;

import java.util.Scanner;
import com.vintud.serviceImpl.*;

public class Main {

	public static void main(String[] args) {

		AdServiceImpl service = new AdServiceImpl();

		Scanner sc = new Scanner(System.in);
		System.out.println("Welcome to Vintud");
		System.out.println("please select menu item");
		System.out.println("1-list all announcements 2- search ab by name 0- Quit");

		boolean quit = false;

		int select;

		do {

			System.out.print("Choose menu item: ");
			select = sc.nextInt();
			switch (select) {
			case 1:
				service.diplayAds();
				break;
			case 2:
				System.out.println("Please insert the name of the announce you want to display");
				String name = sc.next();
				service.getAdsByName(name);
			case 0:
				quit = true;
				break;
			default:
				System.out.println("Invalid choice.");
			}

		} while (!quit);

		System.out.println("Bye-Bye!");
		sc.close();
	}
}
