package com.vintud.daoImpl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.vintud.dao.AdDAO;
import com.vintud.entityImpl.AdImpl;
import com.vintud.utils.ConnectionManager;

public class AdDAOImpl implements AdDAO{
	static ConnectionManager connection = new ConnectionManager();
	static Connection con = connection.getConnection();

	@Override
	public List<AdImpl> getAllAds() {
		List<AdImpl> listAnnouncement = new ArrayList<>();
		Statement st = null;
		try {
			st = con.createStatement();
			String selectAnnouncement = "SELECT * from announcement ";
			ResultSet result = st.executeQuery(selectAnnouncement);
			while (result.next()) {
				AdImpl announcementImpl = new AdImpl();
				announcementImpl.setId(result.getInt("id"));
				announcementImpl.setTitle(result.getString("title"));
				announcementImpl.setDescription(result.getString("description"));
				announcementImpl.setCategory(null);
				announcementImpl.setPublicationDate(result.getDate("publication_date"));
				announcementImpl.setAvailable(result.getBoolean("is_available"));
				announcementImpl.setPrice(result.getDouble("price"));
				announcementImpl.setVuesNbre(result.getInt("view_number"));
				announcementImpl.setLocalisation(result.getString("localisation"));
				announcementImpl.setPicture(result.getBytes("picture"));
				announcementImpl.setStatus(com.vintud.utils.Status.APPROUVED);
				listAnnouncement.add(announcementImpl);
			}
			st.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return listAnnouncement;
		
	}

	@Override
	public List<AdImpl> getAdByName(String name) {
		List<AdImpl> listAnnouncement = new ArrayList<>();
		String query = "SELECT * from announcement WHERE title LIKE ? ";
		try {
			PreparedStatement ps = con.prepareStatement(query);
			ps.setString(1,"%" + name + "%");
			ResultSet result = ps.executeQuery();
			while (result.next()) {
				AdImpl announcementImpl = new AdImpl();
				announcementImpl.setId(result.getInt("id"));
				announcementImpl.setTitle(result.getString("title"));
				announcementImpl.setDescription(result.getString("description"));
				announcementImpl.setCategory(null);
				announcementImpl.setPublicationDate(result.getTimestamp("publication_date"));
				announcementImpl.setAvailable(result.getBoolean("is_available"));
				announcementImpl.setPrice(result.getDouble("price"));
				announcementImpl.setVuesNbre(result.getInt("view_number"));
				announcementImpl.setLocalisation(result.getString("localisation"));
				announcementImpl.setPicture(result.getBytes("picture"));
				announcementImpl.setStatus(com.vintud.utils.Status.APPROUVED);
				listAnnouncement.add(announcementImpl);
			}
			ps.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return listAnnouncement;
	}
	
	
}
