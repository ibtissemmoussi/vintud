	
	package com.vintud.utils;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class ConnectionManagerMysql {

	private   String url = "jdbc:mysql://localhost:3306/vintud";
	private   String driverName = "com.mysql.jdbc.Driver";
	private   String username = "root";
	private   String passWord = "";
 

	ConnectionManagerMysql(String url, String username, String password) {
		this.url = url;
		this.username = username;
		this.passWord = password;
	}

	public ConnectionManagerMysql getInstance() {
		try {
			Class.forName(driverName);
		} catch (ClassNotFoundException e) {

		}
		ConnectionManagerMysql instance = new ConnectionManagerMysql(url, username, passWord);
		return instance;
	}

	public Connection getConnection() throws SQLException {
		return DriverManager.getConnection(url, username, passWord);
	}

}
