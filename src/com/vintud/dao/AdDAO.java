package com.vintud.dao;

import java.util.List;

import com.vintud.entityImpl.AdImpl;


public interface AdDAO {
	
	public List<AdImpl> getAllAds();
	public List<AdImpl> getAdByName(String name);
}
