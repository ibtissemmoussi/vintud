package com.vintud.entityImpl;

import java.util.ArrayList;

import com.vintud.entity.User;

public class UserImpl implements User{

	private int id;
	private String mail;
	private String firstName;
	private String name;
	private String pseudo;
	private String password;
	private String phone;
	private String adress;
	private ArrayList<AdImpl> postedAnnonce = new ArrayList<>();
	private ArrayList<AdImpl> markedAnnonce = new ArrayList<>();
	private RoleImpl role;

	public UserImpl() {
		super();
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((adress == null) ? 0 : adress.hashCode());
		result = prime * result + ((firstName == null) ? 0 : firstName.hashCode());
		result = prime * result + id;
		result = prime * result + ((mail == null) ? 0 : mail.hashCode());
		result = prime * result + ((markedAnnonce == null) ? 0 : markedAnnonce.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + ((password == null) ? 0 : password.hashCode());
		result = prime * result + ((phone == null) ? 0 : phone.hashCode());
		result = prime * result + ((postedAnnonce == null) ? 0 : postedAnnonce.hashCode());
		result = prime * result + ((pseudo == null) ? 0 : pseudo.hashCode());
		result = prime * result + ((role == null) ? 0 : role.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		UserImpl other = (UserImpl) obj;
		if (adress == null) {
			if (other.adress != null)
				return false;
		} else if (!adress.equals(other.adress))
			return false;
		if (firstName == null) {
			if (other.firstName != null)
				return false;
		} else if (!firstName.equals(other.firstName))
			return false;
		if (id != other.id)
			return false;
		if (mail == null) {
			if (other.mail != null)
				return false;
		} else if (!mail.equals(other.mail))
			return false;
		if (markedAnnonce == null) {
			if (other.markedAnnonce != null)
				return false;
		} else if (!markedAnnonce.equals(other.markedAnnonce))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (password == null) {
			if (other.password != null)
				return false;
		} else if (!password.equals(other.password))
			return false;
		if (phone == null) {
			if (other.phone != null)
				return false;
		} else if (!phone.equals(other.phone))
			return false;
		if (postedAnnonce == null) {
			if (other.postedAnnonce != null)
				return false;
		} else if (!postedAnnonce.equals(other.postedAnnonce))
			return false;
		if (pseudo == null) {
			if (other.pseudo != null)
				return false;
		} else if (!pseudo.equals(other.pseudo))
			return false;
		if (role == null) {
			if (other.role != null)
				return false;
		} else if (!role.equals(other.role))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "UserImpl [id=" + id + ", mail=" + mail + ", firstName=" + firstName + ", name=" + name + ", pseudo="
				+ pseudo + ", password=" + password + ", phone=" + phone + ", adress=" + adress + ", postedAnnonce="
				+ postedAnnonce + ", markedAnnonce=" + markedAnnonce + ", role=" + role + "]";
	}

	public UserImpl(int id, String mail, String firstName, String name, String pseudo, String password, String phone,
			String adress, ArrayList<AdImpl> postedAnnonce, ArrayList<AdImpl> markedAnnonce,
			RoleImpl role) {
		super();
		this.id = id;
		this.mail = mail;
		this.firstName = firstName;
		this.name = name;
		this.pseudo = pseudo;
		this.password = password;
		this.phone = phone;
		this.adress = adress;
		this.postedAnnonce = postedAnnonce;
		this.markedAnnonce = markedAnnonce;
		this.role = role;
	}


	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getMail() {
		return mail;
	}

	public void setMail(String mail) {
		this.mail = mail;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPseudo() {
		return pseudo;
	}

	public void setPseudo(String pseudo) {
		this.pseudo = pseudo;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getAdress() {
		return adress;
	}

	public void setAdress(String adress) {
		this.adress = adress;
	}

	public ArrayList<AdImpl> getPostedAnnonce() {
		return postedAnnonce;
	}

	public void setPostedAnnonce(ArrayList<AdImpl> postedAnnonce) {
		this.postedAnnonce = postedAnnonce;
	}

	public ArrayList<AdImpl> getMarkedAnnonce() {
		return markedAnnonce;
	}

	public void setMarkedAnnonce(ArrayList<AdImpl> markedAnnonce) {
		this.markedAnnonce = markedAnnonce;
	}

	public RoleImpl getRole() {
		return role;
	}

	public void setRole(RoleImpl role) {
		this.role = role;
	}

}
