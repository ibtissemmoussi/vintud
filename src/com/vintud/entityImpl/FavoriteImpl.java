package com.vintud.entityImpl;

import java.util.Date;

import com.vintud.entity.Favorite;

public class FavoriteImpl implements Favorite {
	AdImpl ad;
	UserImpl user;
	Date datefavorite;

	public FavoriteImpl(AdImpl ad, UserImpl user, Date datefavorite) {
		super();
		this.ad = ad;
		this.user = user;
		this.datefavorite = datefavorite;
	}

	public AdImpl getAd() {
		return ad;
	}

	public void setAd(AdImpl ad) {
		this.ad = ad;
	}

	public UserImpl getUser() {
		return user;
	}

	public void setUser(UserImpl user) {
		this.user = user;
	}

	public Date getDatefavorite() {
		return datefavorite;
	}

	public void setDatefavorite(Date datefavorite) {
		this.datefavorite = datefavorite;
	}

}
