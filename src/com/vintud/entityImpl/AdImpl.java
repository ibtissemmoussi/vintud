package com.vintud.entityImpl;

import java.util.Arrays;
import java.util.Date;

import com.vintud.entity.Ad;
import com.vintud.utils.Status;

public class AdImpl implements Ad {

	private int id;
	private String title;
	private String description;
	private CategoryImpl category;
	private double price;
	private Date publicationDate;
	private boolean available;
	private int viewsNumbre;
	private String location;
	private byte[] picture;
	private Status status;

	private UserImpl user;


	public AdImpl() {
		super();
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((publicationDate == null) ? 0 : publicationDate.hashCode());
		result = prime * result + (available ? 1231 : 1237);
		result = prime * result + ((category == null) ? 0 : category.hashCode());
		result = prime * result + ((description == null) ? 0 : description.hashCode());
		result = prime * result + id;
		result = prime * result + ((location == null) ? 0 : location.hashCode());
		result = prime * result + Arrays.hashCode(picture);
		long temp;
		temp = Double.doubleToLongBits(price);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		result = prime * result + ((status == null) ? 0 : status.hashCode());
		result = prime * result + ((title == null) ? 0 : title.hashCode());
		result = prime * result + viewsNumbre;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		AdImpl other = (AdImpl) obj;
		if (publicationDate == null) {
			if (other.publicationDate != null)
				return false;
		} else if (!publicationDate.equals(other.publicationDate))
			return false;
		if (available != other.available)
			return false;
		if (category == null) {
			if (other.category != null)
				return false;
		} else if (!category.equals(other.category))
			return false;
		if (description == null) {
			if (other.description != null)
				return false;
		} else if (!description.equals(other.description))
			return false;
		if (id != other.id)
			return false;
		if (location == null) {
			if (other.location != null)
				return false;
		} else if (!location.equals(other.location))
			return false;
		if (!Arrays.equals(picture, other.picture))
			return false;
		if (Double.doubleToLongBits(price) != Double.doubleToLongBits(other.price))
			return false;
		if (status != other.status)
			return false;
		if (title == null) {
			if (other.title != null)
				return false;
		} else if (!title.equals(other.title))
			return false;
		if (viewsNumbre != other.viewsNumbre)
			return false;
		if (user == null) {
			if (other.user != null)
				return false;
		} else if (!user.equals(other.user))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "\n Ad # " +id+":"+
				"title=" + title +" "+
				"description=" + description +" "+
				"price=" + price +" ";
	}

	public AdImpl(int id, String title, String description, CategoryImpl category, double price,
	Date publicationDate, boolean available, int vuesNbre, String localisation, byte[] picture, Status status,UserImpl user) {
		

		super();
		this.id = id;
		this.title = title;
		this.description = description;
		this.category = category;
		this.price = price;
		this.publicationDate = publicationDate;
		this.available = available;
		this.viewsNumbre = vuesNbre;
		this.location = localisation;
		this.picture = picture;
		this.status = status;

		this.user = user;

	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public CategoryImpl getCategory() {
		return category;
	}

	public void setCategory(CategoryImpl category) {
		this.category = category;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public Date getPublicationDate() {
		return publicationDate;
	}

	public void setPublicationDate(Date publicationDate) {

		this.publicationDate = publicationDate;

	}

	public boolean isAvailable() {
		return available;
	}

	public void setAvailable(boolean available) {
		this.available = available;
	}

	public int getVuesNbre() {
		return viewsNumbre;
	}

	public void setVuesNbre(int vuesNbre) {
		this.viewsNumbre = vuesNbre;
	}

	public String getLocalisation() {
		return location;
	}

	public void setLocalisation(String localisation) {
		this.location = localisation;
	}

	public byte[] getPicture() {
		return picture;
	}

	public void setPicture(byte[] b) {
		this.picture = b;

	}

	public Status getStatus() {
		return status;
	}

	public void setStatus(Status status) {
		this.status = status;
	}

	 

	/**
	 * @return the user
	 */
	public UserImpl getUser() {
		return user;
	}

	/**
	 * @param user the user to set
	 */
	public void setUser(UserImpl user) {
		this.user = user;
	}

	 
	
	
}
